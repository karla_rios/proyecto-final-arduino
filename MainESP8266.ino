
// --- pines a usar
#define pin_led 5 // D1(GPIO5 ) el nombre de la variable es pin_led, y el valor es 5, es porque el valor de d1 es 5, ejemplo: var pin_led = 5; 
#define sensor A0

// ****** INCLUIR LAS LIBRERIAS A USAR
#include <ESP8266WiFi.h>
#include <WiFiClient.h>
#include <ESP8266HTTPClient.h>
#include <FirebaseArduino.h>
#include <ArduinoJson.h>


// ----------------- DEFINICIÓN DE LAS VARIABLES A USAR

// --- Parametros de red
/*
String ssid = "INFINITUMnckt"; // RED a donde se va a conectar
String password = "bc50522ae2"; //  CONTRASEÑA DE A RED
*/

String ssid = "INFINITUM3N20_2.4"; // RED a donde se va a conectar
String password = "9pRR6gav4W"; //  CONTRASEÑA DE A RED

#define FIREBASE_HOST "iot-app-proyecto-final-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "3rzmmbgwMWKEU7BgOqaPXSz0gWTcssikoz18m28d"

int tiempoMedicion = 10; //cada segundos
unsigned long tiempoAntMedicion = 0;

int tiempoLect = 5;
unsigned long tiempoAntLect = 0;

int umbral = 35;// 196;// aqui vamos a cambiar el umbra a uno de los valores que nos salen a nosotros
int temperatura = 0;

bool manualControl = false;

bool changed = false;

void setup() {
  // put your setup code here, to run once:

  Serial.begin(9600);
  // Serial.println("Hola mundo");


  // 2.- Conectar al wifi
  WiFi.begin(ssid, password);// inicia la conexion
  while (WiFi.status() != WL_CONNECTED) {
    Serial.println("Conectando...");
    delay(500);
  }

  Serial.println("********************");
  delay(500);
  Serial.println("Wifi conectado");
  delay(500);
  Serial.println("Ip de la placa");
  delay(500);
  Serial.println(WiFi.localIP());
  delay(500);
  Serial.println("********************");
  delay(500);

  // conectar firebase
  Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
  /*
     Pobar la conexion de firebase
    int Version = Firebase.getInt("version");
    if (Firebase.failed()){
      Serial.println("Conexion con firebase fallida");
      Serial.println(Firebase.error());
     }else{
        Serial.println("Conexion a firebase exitosa");
        Serial.println(Version);
     }
  */
  //3.- Definir los modos de entrada o salida, Cada puerto sera de entrada o salida?
  pinMode(pin_led, OUTPUT); // se indica que es un pin de salida
  // pinMode(sensor, INPUT);

  Firebase.stream("version");

}

void loop() {
  // put your main code here, to run repeatedly:

  /*
    digitalWrite(pin_led, HIGH); // enviar voltaje
    delay(1000);  // esperar un sgundo asi
    digitalWrite(pin_led, LOW);  // enviar nada de voltaje
    delay(1000);  // espera asi  segundo
    Serial.println("Holi");
     digitalWrite(pin_led, HIGH); // encender led
    delay(5000); // espera 5 segundos
    digitalWrite(pin_led, LOW); // apagar led
    delay(1000);// esperar un segundo
  */

  getTemperatura();

  uploadStateVentilador();
  SubscriptionChanges();
  uploadTemperatura(temperatura); // guardar el valor de la temperatura
  //String Time = getTime();
  //Serial.println(temperatura);
  //delay(5000);
  //Serial.println(Time);
  // delay(5000);



}// fin loop()

int getTemperatura() {
  //Variables para realizar la ecuación Steinhart-Hart
float temp; //Elementos para realizar la ecuación
  if (millis() - tiempoAntLect >= tiempoLect * 1000)  {
    tiempoAntLect = millis();
    temperatura = analogRead(sensor);    // 0 - 1023
    //temperatura  = map(temperatura, 0, 1023, 0, 50);
    //------ Conversión de valores analógicos a grados Celsius ----------------
   /* float r, rcubo;
    r = log(10000.0*((1023.0/ (float)temp - 1.0)));//Calculo de la resistencia del termistor
    rcubo = r*r*r;
    temperatura = 1.0/ (0.001129148 + (0.000234125 * r) + (rcubo*0.0000000876741)); // Temperatura en Kelvin
    temperatura = (temperatura - 273.15)* (-1);//Conversión a Temperatura Celsius
    temperatura = (temp - 273.15)* (-1);
    //TC = TC * (-1);
    //temperatura = TC;*/
     temperatura = (temperatura - 273.15)* (-1);
    if (manualControl == false) {
      if (temperatura >= umbral) {
        setVentilador(true);
      } else {
        setVentilador(false);
      }
    }

  }
  return temperatura;
}

String getTime() {
  WiFiClient client;
  HTTPClient http;
  String timeS = "";

  if (http.begin(client, "http://worldtimeapi.org/api/timezone/America/Mexico_City" )) {
    int httpCode = http.GET();
    // Serial.println(httpCode);
    if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
      //Serial.println("Entra aqui");

      String payload = http.getString();
      int beginS = payload.indexOf("datetime");
      int endS = payload.indexOf("day_of_week");
      // Serial.println(payload);
      timeS = payload.substring(beginS + 11, endS - 3);
      Serial.println(timeS);

    }

  }
  return timeS;

}

void  uploadTemperatura ( int value) {
 
  if ( millis () - tiempoAntMedicion >= tiempoMedicion * 1000 ) {
    // subimos temperatura al servidor
    tiempoAntMedicion = millis();
    Serial. print ( "uploadTemperatura -> " );
    Serial. println (value);
    String Time = getTime();
    String path = "mediciones/" ;
    StaticJsonBuffer<200> jsonBuffer;
    JsonObject& data = jsonBuffer.createObject();
    data[ "time" ] = Time;
    data["sensor" ] = value;
    Firebase.push(path, data);

  }

}




void setVentilador(bool estado) {
  bool estadoAct = digitalRead(pin_led);
  if (estadoAct != estado) {
    digitalWrite(pin_led, estado);
    Serial.print("set ventilador: ");
    Serial.println(estado);
    changed = true;
  }
}

void uploadStateVentilador() {
  if (changed) {
    bool estado = digitalRead(pin_led);
    Firebase.setBool("ventilador_state", estado);
    if (!Firebase.failed()) {
      Serial.println("write success");
      changed = false;
    } else {
      Serial.println("write failed");
    }
    delay(1000);
  }

}

void SubscriptionChanges() {
  if (Firebase.available()) {
    FirebaseObject event = Firebase.readEvent();
    String eventType = event.getString("type");
    if (eventType == "put") {
      Serial.println("|....Changes() DataBase....|");
      int value = Firebase.getInt("ventilador");
      if (!Firebase.failed()) {
        Serial.print("ventilador configurado: ");
        Serial.println(value);
        if (value == 0) {
          manualControl = false;
        }
        if (value > 0) {
          manualControl = true;
          if (value == 1) {
            setVentilador(false);
          }
          if (value == 2) {
            setVentilador(true);
          }
        }
      }
      umbral = Firebase.getInt("umbral");
      if (!Firebase.failed()) {
        Serial.print("umbral configurado: ");
        Serial.println(umbral);
      }
    }
  }
}
/*****
  Notas importantessssss

  @ kimleng-ly puede verificar la huella digital a través de este sitio https://www.grc.com/fingerprints.htm?domain=yourdatabase.firebaseio.com

  Para la prueba, puede utilizar https://www.grc.com/fingerprints.htm?domain=test.firebaseio.com .

  Si lees mi publicación anterior, la solución 1 (si tienes conocimientos de PHP) es para actualizar automáticamente la huella digital cuando tu dispositivo comienza a conectarse a firebase.

  En caso de que desee omitir la verificación de huellas dactilares, consulte la solución 2 de mi publicación anterior.


*/
